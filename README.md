# Week 4 Mini-Project

##  Requirements
Containerize simple Rust Actix web app

Build Docker image

Run container locally

## Install Docker
Install docker [Docker](https://docs.docker.com/desktop/install/mac-install/)

## Verify Docker
Use the following command to test whether Docker has been installed well on your system.
```
sudo docker run hello-world
```
If the terminal appeals this means that Docker has been well installed.
```
Unable to find image 'hello-world:latest' locally
latest: Pulling from library/hello-world
b8dfde127a29: Pull complete
Digest: sha256:308866a43596e83578c7dfa15e27a73011bdd402185a84c5cd7f32a88b501a24
Status: Downloaded newer image for hello-world:latest

Hello from Docker!
This message shows that your installation appears to be working correctly.

To generate this message, Docker took the following steps:
 1. The Docker client contacted the Docker daemon.
 2. The Docker daemon pulled the "hello-world" image from the Docker Hub.
    (amd64)
 3. The Docker daemon created a new container from that image which runs the
    executable that produces the output you are currently reading.
 4. The Docker daemon streamed that output to the Docker client, which sent it
    to your terminal.

To try something more ambitious, you can run an Ubuntu container with:
 $ docker run -it ubuntu bash

Share images, automate workflows, and more with a free Docker ID:
 https://hub.docker.com/

For more examples and ideas, visit:
 https://docs.docker.com/get-started/
```

## Create a Rust web application
1. Create a template by
    ```
    cargo new actix_web_app
    ```
2. Add corresponding dependencies and bin into `Cargo.toml`
3. Add functions in the `src/main.rs` file
4. Create Dockerfile and add corresponding contents
5. Build Docker image using `sudo docker build -t <YOUR IMAGE NAME> .` If error occurs, try `docker pull <ERROR>` before building.
![image](Build Docker Image.png)

6. Docker Image created
![image](Docker Image Created.png)

7. Run `docker run -d -p 8080:8080 <YOUR IMAGE NAME>`, check if the container is created in Docker Desktop.
![image](Container.png)

## To test locally
Build the project. Run the project in the terminal. 
```
cargo build
cargo run
```
In a separate terminal session, run `curl 'localhost:8080'` to see if it works locally.
![image](Test Locally.png)
![image](Run Locally.png)
